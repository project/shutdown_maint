INTRODUCTION
------------

By default, Drupal's maintenance mode still permits users to log in, so long
as they have the correct permissions.

This module hardens the system: users may not log in; any user who is logged
in is logged out.  Unless other modules intervene, only the maintenance page
is displayed on any path.

This module permits a site builder to shut down Drupal in the event of a
security concern that implicates the log in function.

REQUIREMENTS
------------

You must be able to turn maintenance mode off from the command line (for
example, by running "drush vset maintenance_mode 0").  Remember that you will
usually need to clear caches ("drush cc all") after putting the site back
online.

BEWARE: If you cannot turn off maintenance mode from the command line, and this
module is enabled, you will be up a creek without a paddle.
